package com.example.mvvm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.mvvm.model.Owner
import com.example.mvvm.model.Repositories
import com.example.mvvm.repository.MainRepository
import com.example.mvvm.utils.Resource
import com.example.mvvm.utils.Resource.Companion.success
import com.example.mvvm.utils.Status
import com.example.mvvm.viewmodel.HomeViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class NewsViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = MainCoroutineScopeRule()

    @Mock
    private lateinit var mockObserver: Observer<Resource<List<Repositories>>>

    private lateinit var myViewModel: HomeViewModel

    @Mock
    private lateinit var useCase: MainRepository

    private lateinit var viewState: Resource<List<Repositories>>

    @Captor
    private lateinit var captor: ArgumentCaptor<Resource<List<Repositories>>>


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewState = Resource(Status.LOADING, null, null)
        myViewModel = HomeViewModel(useCase)
    }

    @Test
    fun assertSuccess() {
        coroutineScope.runBlockingTest {
            val data = listOf(
                Repositories(
                    "name 1",
                    "description",
                    "2020-08-20T08:11:09Z",
                    19,
                    20,
                    Owner("url")
                ),
                Repositories(
                    "name 1",
                    "description",
                    "2020-08-20T08:11:09Z",
                    19,
                    20,
                    Owner("url")
                ),
                Repositories(
                    "name 1",
                    "description",
                    "2020-08-20T08:11:09Z",
                    19,
                    20,
                    Owner("url")
                )
            )
            val flow = flow {
                emit(success(data))
            }

            `when`(useCase.getGitHubUserData("id")).thenReturn(flow)
            val liveData = myViewModel.getGitHubUserData("id")
            liveData.observeForever(mockObserver)
            verify(mockObserver).onChanged(captor.capture())
            assertEquals(
                "name", captor.value.data?.get(0)?.name
            )
        }
    }

    @Test
    fun assertFailure() {
        coroutineScope.runBlockingTest {
            val flow = flow {
                emit(Resource.error(null, "error occurred"))
            }
            `when`(useCase.getGitHubUserData("id")).thenReturn(flow)
            val liveData = myViewModel.getGitHubUserData("id")
            liveData.observeForever(mockObserver)
            verify(mockObserver).onChanged(captor.capture())
            assertEquals(
                null, captor.value.data
            )
        }
    }
}