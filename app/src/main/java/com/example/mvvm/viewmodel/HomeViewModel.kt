package com.example.mvvm.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.model.Repositories
import com.example.mvvm.repository.MainRepository
import com.example.mvvm.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect


class HomeViewModel @ViewModelInject constructor(private val mainRepository: MainRepository) : ViewModel(), LifecycleObserver {

    var team: MutableLiveData<Resource<List<Repositories>>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    suspend fun getGitHubUserData(id: String) : LiveData<Resource<List<Repositories>>> {
        mainRepository.getGitHubUserData(id).collect {
            team.value = it
        }
        return team
    }

}