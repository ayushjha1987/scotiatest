package com.example.mvvm.api

import com.example.mvvm.model.Repositories
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface ApiService {

    @Headers("application-type: REST", "Content-Type: application/json")
    @GET("{id}/repos")

    suspend fun getGitHubUserData(@Path("id")id: String): List<Repositories>

}