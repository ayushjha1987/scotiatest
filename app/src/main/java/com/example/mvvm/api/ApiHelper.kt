package com.example.mvvm.api

import retrofit2.http.Query
import javax.inject.Inject

class ApiHelper @Inject constructor(private val apiService: ApiService) {

    suspend fun getGitHubUserData(@Query("userId") id: String) = apiService.getGitHubUserData(id)
}