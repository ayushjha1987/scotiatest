package com.example.mvvm.repository

import com.example.mvvm.api.ApiHelper
import com.example.mvvm.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    @ExperimentalCoroutinesApi
    fun getGitHubUserData(id: String) = flow {
        emit(Resource.loading(null))
        val response = apiHelper.getGitHubUserData(id)
        if (response.isNotEmpty()) {
            emit(Resource.success(response))
        } else {
            emit(Resource.error(null, "Http Error!"))
        }
    }

}