package com.example.mvvm.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mvvm.R
import com.example.mvvm.model.Repositories
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_layout.*
import java.text.SimpleDateFormat
import java.util.*

class CustomBottomSheetDialogFragment(repositories: Repositories) : BottomSheetDialogFragment() {
    var repo = repositories
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutLastUpdated.setLeftText(getString(R.string.last_updated))
        layoutForks.setLeftText(getString(R.string.forks))
        layoutStars.setLeftText(getString(R.string.stars))
        layoutLastUpdated.setRightText(repo.updated_at?.let { getDate(it) })
        layoutForks.setRightText(repo.forks.toString())
        layoutStars.setRightText(repo.stargazers_count.toString())
    }

    private fun getDate(timestamp: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val parsedDate = sdf.parse(timestamp)
        val parser = SimpleDateFormat("MMMM dd, yyyy hh:MM:ss a")
        parser.timeZone = TimeZone.getTimeZone("UTC")
        return parser.format(parsedDate)
    }
}