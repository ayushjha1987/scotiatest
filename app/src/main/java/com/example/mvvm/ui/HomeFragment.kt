package com.example.mvvm.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.adapter.DataAdapter
import com.example.mvvm.model.Repositories
import com.example.mvvm.utils.Resource
import com.example.mvvm.utils.Status.*
import com.example.mvvm.viewmodel.HomeViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
class HomeFragment : Fragment(), CoroutineScope {

    private lateinit var adapter: DataAdapter
    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var recyclerView: RecyclerView
    private var job: Job = Job()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        setupUI(root)
        setupObservers()
        return root
    }

    private fun setupUI(root: View) {
        recyclerView = root.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        adapter = DataAdapter()
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
    }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchButton.setOnClickListener {
            searchButton.hideKeyboard()
            launch {
                homeViewModel.getGitHubUserData(searchField.text.toString())
            }
        }
    }

    private fun setupObservers() {
        val observer = Observer<Resource<List<Repositories>>> {
            it.let { resource ->
                when (resource.status) {
                    SUCCESS -> {
                        recyclerView.visibility = View.VISIBLE
                        resource.data?.let { repos ->
                            imageView.visibility = View.VISIBLE
                            retrieveList(repos)
                            activity?.let { fragmentActivity ->
                                Glide.with(fragmentActivity).load(repos[0].owner.avatar_url)
                                    .into(imageView)
                            };
                        }
                    }
                    ERROR -> {
                        recyclerView.visibility = View.GONE
                        view?.let { view -> Snackbar.make(view,"Error Occurred",Snackbar.LENGTH_SHORT).show() }
                    }
                    LOADING -> {
                        imageView.visibility = View.GONE
                        recyclerView.visibility = View.GONE
                    }
                }
            }
        }
        homeViewModel.team.observe(viewLifecycleOwner, observer)
    }

    fun showBottomNavigation(repositories: Repositories) {
        bottomLayout.visibility = View.VISIBLE
        val bottomSheet = CustomBottomSheetDialogFragment(repositories)
        activity?.supportFragmentManager?.let { bottomSheet.show(it, "BottomSheetEx") }
    }

    private fun retrieveList(repos: List<Repositories>) {
        adapter.apply {
            addTeams(repos, this@HomeFragment)
            notifyDataSetChanged()
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
}