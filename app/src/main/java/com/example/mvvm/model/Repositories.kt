package com.example.mvvm.model

data class Repositories(
    var name: String? = null,
    var description: String? = null,
    var updated_at: String? = null,
    var forks: Int? = null,
    var stargazers_count: Int? = null,
    var owner: Owner
)