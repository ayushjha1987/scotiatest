package com.example.mvvm.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.mvvm.R


class BottomSheetLineView : LinearLayout {
    var layout: LinearLayout? = null
    var leftTextView: TextView? = null
    var rightTextView: TextView? = null
    var mContext: Context? = null

    constructor(context: Context): super(context){
        mContext = context
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        mContext = context
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        mContext = context
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.DoubleText)
        var leftText = a.getString(R.styleable.DoubleText_leftText)
        var rightText = a.getString(R.styleable.DoubleText_rightText)
        leftText = leftText ?: ""
        rightText = rightText ?: ""
        val service: String = Context.LAYOUT_INFLATER_SERVICE
        val li = getContext().getSystemService(service) as LayoutInflater
        layout = li.inflate(R.layout.bottom_sheet_view, this, true) as LinearLayout?
        leftTextView = layout?.findViewById<View>(R.id.lastUpdated) as TextView
        rightTextView = layout?.findViewById<View>(R.id.lastUpdatedValue) as TextView
        leftTextView?.text = leftText
        rightTextView?.text = rightText
        a.recycle()
    }

    fun setLeftText(text: String?) {
        leftTextView!!.text = text
    }

    fun setRightText(text: String?) {
        rightTextView!!.text = text
    }

    fun getLeftText(): String? {
        return leftTextView!!.text.toString()
    }

    fun getRightText(): String? {
        return rightTextView!!.text.toString()
    }
}