package com.example.mvvm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.R
import com.example.mvvm.model.Repositories
import com.example.mvvm.ui.HomeFragment
import java.util.*


class DataAdapter : RecyclerView.Adapter<DataAdapter.ViewHolder>() {
    private lateinit var home: HomeFragment
    private var description = ArrayList<Repositories>()
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_layout, viewGroup, false)
        return ViewHolder(view)
    }

    fun addTeams(team: List<Repositories>, homeFragment: HomeFragment) {
        this.description.apply {
            clear()
            addAll(team)
        }
        home = homeFragment

    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.teamName.text = description[i].name
        viewHolder.wins.text = description[i].description
        viewHolder.layout.setOnClickListener {
            home.showBottomNavigation(description[i])
        }
    }

    override fun getItemCount(): Int {
        return description.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val wins: TextView = view.findViewById<View>(R.id.description) as TextView
        val teamName: TextView = view.findViewById<View>(R.id.name) as TextView
        val layout: LinearLayout = view.findViewById<View>(R.id.layout) as LinearLayout
    }

}